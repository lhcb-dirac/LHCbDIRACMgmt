"""
This script can be used to reindex data from a source cluster tp the destination cluster.
"""

# imports
import multiprocessing
from elasticsearch.helpers import reindex

from DIRAC import gLogger
from DIRAC.Core.Utilities.ElasticSearchDB import ElasticSearchDB

gLogger.setLevel('VERBOSE')

# coordinates
localTests = {
    'server': 'localhost',
    'port': 9200,
    'useSSL': False,
    'indecesPatternFrom': 'test_',
    'indecesTo': 'test_combined'
}


cert = {
    'server': 'es-lhcb-dev.cern.ch',
    'port': 9203,
    'indecesPatternFrom': '',
    'indecesTo': ''
}

WMSHistory = {
    'server': '',
    'mapping': {
        'WMSHistory': {'properties': {'ApplicationStatus': {'type': 'keyword'},
                       'JobGroup': {'type': 'keyword'},
                       'JobSplitType': {'type': 'keyword'},
                       'MinorStatus': {'type': 'keyword'},
                       'Site': {'type': 'keyword'},
                       'Status': {'type': 'keyword'},
                       'User': {'type': 'keyword'},
                       'UserGroup': {'type': 'keyword'},
                       'timestamp': {'type': 'date'}}}}
}


# ## On what to operate [EDIT HERE]

whatToReIndex = localTests

# ##


# we need a list of indexes for reindex.
esDB = ElasticSearchDB(whatToReIndex['server'], whatToReIndex['port'],
                       user=whatToReIndex.get('user', None),
                       password=whatToReIndex.get('password', None),
                       useSSL=whatToReIndex.get('useSSL', True),
                       indexPrefix=whatToReIndex.get('indicesPatternFrom', ''))


indecesToReIndex = esDB.getIndexes()
indecesToReIndex.reverse()


def do_reindex(sourceIndeces, destinationIndex):
  if not esDB.exists(destinationIndex):
    res = esDB.createIndex(destinationIndex,
                           mapping=whatToReIndex.get('mapping', None),
                           period=None)
    if not res['OK']:
      print("Problem creating destination index", destinationIndex)
      print res['Message']
      exit(1)

  print ("Indexing", sourceIndeces)
  res = reindex(esDB.client, sourceIndeces, destinationIndex, target_client=esDB.client)
  print("Reindexed:", sourceIndeces, multiprocessing.current_process().name, res)


if __name__ == '__main__':
  print whatToReIndex
  print indecesToReIndex
  do_reindex(indecesToReIndex, whatToReIndex.get('indecesTo', ''))
