""" Testing the cfg parser"""

import logging
import urllib2
import unittest
import os

from lbmesos.configuration import CfgManagement

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

test_dir = os.path.dirname(__file__)


class test_generic(unittest.TestCase):
  """ Tests  getMasterURLs on all the config files
  """

  def test_nonExistingConfig(self):
    """" Non existing file"""
    with self.assertRaises(Exception):
      CfgManagement('IDontExist.json')

  def test_noVersion(self):
    """ Test we cannot read a file when no version is defined """
    with self.assertRaises(KeyError):
      CfgManagement(test_dir + '/testConfigs/empty.json')

  def test_noMasterURLs(self):
    """ Test several failure cases """

    # Try getting the masters a non defined template
    cfg = CfgManagement(test_dir + '/testConfigs/versionOnly.json')
    with self.assertRaises(KeyError):
      cfg.getMasterURLs('NonDefinedTemplate')

  def test_undefinedTemplate(self):
    """ Try a non defined template """
    cfg = CfgManagement(test_dir + '/testConfigs/undefined_template.json')
    with self.assertRaises(urllib2.URLError):
      cfg.getApplications('Marathon')

  def test_specialVariable(self):
    """ test the resolution of the default variables passed to the template"""
    cfg = CfgManagement(test_dir + '/testConfigs/resolveVariable.json')
    apps = sorted(cfg.getApplications()['Marathon'])

    expectedApps = sorted([{
        'service_id': '/level1/sub',
        'node_name': 'Sub'
    }, {
        'service_id': '/level1',
        'node_name': 'Level1'
    }, {
        'service_id': '/level2/node',
        'node_name': 'Node'
    }])

    self.assertEqual(len(apps), len(expectedApps))

    for appId, app in enumerate(expectedApps):
      self.assertDictEqual(app, apps[appId])


class TestConfig1(unittest.TestCase):
  """ Perform tests on config1 """

  def setUp(self):
    """set up"""
    self.cfg = CfgManagement(test_dir + '/testConfigs/config1.json')

  def test_getMasterURLs(self):
    """ Get the master URLs"""

    masterURLs = self.cfg.getMasterURLs('Marathon')
    expectedURLs = ['https://host1:1234', 'https://host2:1234']

    self.assertListEqual(masterURLs, expectedURLs)

  def assertDictEqual(self, d1, d2, msg = None):  # assertEqual uses for dicts
    """ Recursive testing """
    for k, v1 in d1.iteritems():
      self.assertIn(k, d2, msg)
      v2 = d2[k]
      import collections
      if isinstance(v1, collections.Iterable) and not isinstance(v1, basestring):
        self.assertItemsEqual(v1, v2, msg)
      else:
        self.assertEqual(v1, v2, msg)
    return True

  def test_getListOfTasks(self):
    """ Test the list of tasks"""

    # LHCbDIRAC is invisible because starts with _
    expectedTaskLists = {'Marathon': ['/DataManagement/Bookkeeping', '/DataManagement/FileCatalog']}
    taskLists = self.cfg.getTaskLists()
    self.assertDictEqual(taskLists, expectedTaskLists)

    taskLists = self.cfg.getTaskLists(framework = 'Marathon')
    self.assertDictEqual(taskLists, expectedTaskLists)

  def test_getInterpretedTasks(self):
    """ Test the interpretation """

    expectedInterpretedTasks = {
        'Marathon': [{
            'mem': 2,
            'cmd': None,
            'id': '/datamanagement/filecatalog'
        }, {
            'mem': 3,
            'cmd': None,
            'id': '/datamanagement/bookkeeping'
        }]
    }

    interpretedTasks = self.cfg.getApplications()

    self.assertDictEqual(interpretedTasks, expectedInterpretedTasks)
