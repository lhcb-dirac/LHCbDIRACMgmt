#!/usr/bin/env python
# Run with PYTHONUNBUFFERED=1
from __future__ import print_function

import json
import os
from subprocess import check_output, CalledProcessError

import sys
print('sys.argv=', sys.argv)

RESULT = {
    'os.environ': dict(os.environ),
}


def run_test(command, lhcb_env=False):
    # assert isinstance(command, list), command
    print('##### Running test with', command)
    if lhcb_env:
        command = 'source /cvmfs/lhcb.cern.ch/lib/LbEnv-unstable; ' + command

    try:
        returncode = 0
        output = check_output(command, shell=lhcb_env)
    except CalledProcessError as e:
        output = e.output
        returncode = e.returncode
    except OSError:
        returncode = 10000000000
        output = 'Got OSError'
    print('Test exited with code', returncode)
    print('Test output was', output)
    if isinstance(command, list):
        RESULT[' '.join(command)] = returncode, output
    else:
        RESULT[command] = returncode, output


run_test(['env'])
run_test('ls -la /cvmfs/lhcb.cern.ch'.split())
run_test('ls -la /cvmfs/lhcbdev.cern.ch'.split())
run_test('ls -la /cvmfs/sft.cern.ch'.split())
run_test('ls -la /cvmfs/cernvm-prod.cern.ch'.split())
run_test('ls -la /cvmfs/cernvm-prod.cern.ch/cvm3'.split())
run_test('ls -la /cvmfs/cernvm-prod.cern.ch/cvm4'.split())
run_test('which singularity'.split())
run_test('singularity --version'.split())
run_test('singularity --help'.split())
run_test('singularity exec --bind /cvmfs --no-home /cvmfs/cernvm-prod.cern.ch/cvm3 env'.split())
run_test('singularity exec --bind /cvmfs --no-home /cvmfs/cernvm-prod.cern.ch/cvm4 env'.split())
run_test('lb-describe-platform', lhcb_env=True)
run_test('which lb-run', lhcb_env=True)
run_test('lb-run --version', lhcb_env=True)
run_test('lb-run --debug -c best --allow-containers --prefer-container Gaudi/v23r0 gaudirun.py', lhcb_env=True)

with open('singularity-check-result.json', 'wt') as fp:
    json.dump(RESULT, fp)
