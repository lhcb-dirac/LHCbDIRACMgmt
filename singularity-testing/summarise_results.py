#!/usr/bin/env python
from collections import defaultdict
import glob
import json
from pprint import pprint
import re
import os

from tabulate import tabulate

if 'JOB_NAME' not in os.environ:
    raise KeyError('JOB_NAME environment variable must be set')
job_name = os.environ['JOB_NAME']

if not os.path.isdir(job_name):
    raise FileNotFoundError(f'Failed to find directory of output sandboxes called '
                            f'"{job_name}", did you run dirac-wms-job-submit.py?')
fns = glob.glob(f'{job_name}/*/singularity-check-result.json')
assert fns, fns


def check_job(fn, key):
    with open(fn, 'rt') as fp:
        result = json.load(fp)
    job_env = result['os.environ']
    return result, job_env['DIRACSITE'], result[key]


tier_1s = [
    "LCG.CERN.cern",
    "LCG.CNAF.it",
    "LCG.GRIDKA.de",
    "LCG.IN2P3.fr",
    "LCG.NIKHEF.nl",
    "LCG.PIC.es",
    "LCG.RAL.uk",
    "LCG.RRCKI.ru",
    "LCG.SARA.nl",
]

headings = [
    'Site',
    'Platform',
    'which singularity',
    'singularity version',
    'Has CVM3?',
    'Run CVM3?',
    'Has CVM4?',
    'Run CVM4?',
    'Detected singularity?',
    'SLC5 Gaudi works?',
    'All okay?',
]


def run(show_tier_1s=None):
    results = defaultdict(lambda: [set() for h in headings[1:]])
    no_singularity = set()
    no_cvmfs = set()

    for fn in fns:
        with open(fn, 'rt') as fp:
            result = json.load(fp)
        job_env = result['os.environ']
        site = job_env['DIRACSITE']
        if show_tier_1s is None:
            pass
        elif show_tier_1s:
            if show_tier_1s and site not in tier_1s:
                continue
        else:
            if show_tier_1s and site in tier_1s:
                continue

        code, output = result['source /cvmfs/lhcb.cern.ch/lib/LbEnv-unstable; lb-describe-platform']
        platform = re.findall(r'dirac_platform: ([^\n+]+)', output)
        assert len(platform) == 1, platform
        results[site][0].add(platform[0])

        code, output = result['which singularity']
        results[site][1].add(output.strip())

        code, output = result['singularity --version']
        if code == 0:
            results[site][2].add(output.strip().replace('singularity version ', ''))
        else:
            no_singularity.add(site)

        code, output = result['ls -la /cvmfs/cernvm-prod.cern.ch/cvm3']
        results[site][3].add(code == 0)
        if code != 0:
            no_cvmfs.add(site)

        code, output = result['singularity exec --bind /cvmfs --no-home /cvmfs/cernvm-prod.cern.ch/cvm3 env']
        results[site][4].add(code == 0)

        code, output = result['ls -la /cvmfs/cernvm-prod.cern.ch/cvm4']
        results[site][5].add(code == 0)

        code, output = result['singularity exec --bind /cvmfs --no-home /cvmfs/cernvm-prod.cern.ch/cvm4 env']
        results[site][6].add(code == 0)

        code, output = result['source /cvmfs/lhcb.cern.ch/lib/LbEnv-unstable; lb-describe-platform']
        containers_supported = '/cvmfs/cernvm-prod.cern.ch/cvm3' in output
        assert (containers_supported) == ('/cvmfs/cernvm-prod.cern.ch/cvm4' in output)
        results[site][7].add(containers_supported)

        code, output = result['source /cvmfs/lhcb.cern.ch/lib/LbEnv-unstable; lb-run --debug -c best --allow-containers --prefer-container Gaudi/v23r0 gaudirun.py']
        results[site][8].add(code == 0)

        log_fn = fn.replace('singularity-check-result.json', 'StdErr')
        with open(log_fn, 'rt') as fp:
            log = fp.read()
        ran_with_container = '--container=singularity' in log
        all_ok = not containers_supported or code == 0
        if not all_ok:
            print('Issue with site!!!', site, fn, log_fn)
        results[site][9].add(all_ok)

        # if site == 'LCG.Lancaster.uk':
        #     print(fn, 'all_ok =', all_ok)

    # results = {k: v for k, v in results.items()
    #            if k not in no_cvmfs and k not in no_singularity}

    table = tabulate(sorted([[k]+v[1:] for k, v in results.items()]), headers=['Site']+headings[2:])
    table = table.replace('True', '\033[32;1mTrue\033[0m')
    table = table.replace('False', '\033[31;1mFalse\033[0m')
    table = table.replace("{''}", "\033[31;1m{''}\033[0m")
    table = table.replace("set()", "\033[31;1mset()\033[0m")
    table = table.replace("singularity'", "\033[32;1msingularity\033[0m'")
    print(table)

    table = tabulate(sorted([[k]+v[:1] for k, v in results.items()]), headers=['Site']+headings[1:2])
    table = table.replace('centos7', '\033[32;1mcentos7\033[0m')
    table = table.replace('slc6', '\033[31;1mslc6\033[0m')
    table = table.replace('unknown', '\033[33;1munknown\033[0m')
    print(table)

    return results, no_singularity, no_cvmfs


# results, no_singularity, no_cvmfs = run(show_tier_1s=True)
# results, no_singularity, no_cvmfs = run(show_tier_1s=False)
results, no_singularity, no_cvmfs = run()
print('Singularity is not installed at', *no_singularity, sep='\n    * ')
print('CERN VM CVMFS is not mounted at', *no_cvmfs, sep='\n    * ')
