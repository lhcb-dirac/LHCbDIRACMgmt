# Testing singularity support on the grid

```bash
# Edit check_singularity.jdl and summarise_results.py to match your chosen job name
export JOB_NAME=cburr.CheckSing.v2

# Prepare the jobs
mkdir jdl
python generate_jdl.py

# Submit jobs
for jdl in jdl/*.jdl; do dirac-wms-job-submit.py $jdl done

# Download and summarise the results
dirac-wms-job-get-output.py -g ${JOB_NAME}
python summarise_results.py
```
