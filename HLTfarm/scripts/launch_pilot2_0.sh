export PATH
#  force the USER
export USER=lhcbprod
# force the location of the USER
export HOME=/home/lhcbprod
ulimit -u 5120
ps afx | grep diracmon | grep -v grep
if [ $? -eq 1 ] ; then
  echo exit
fi

WORKDIR=false

if [ -r /localdisk1/dirac ]; then
  WORK_DIR=/localdisk1/dirac/work
  if [ ! -d "$WORK_DIR" ]; then
    mkdir -p $WORK_DIR
    if [ ! $? -eq 0 ]; then
      echo "Work dir '"$WORK_DIR"' could not be created"
    else
      WORKDIR=true
    fi
  else
    WORKDIR=true
  fi
fi

if [ $WORKDIR == false ]; then
  if [ -r /localdisk2/dirac ] ; then
    WORK_DIR=/localdisk2/dirac/work
    if [ ! -d "$WORK_DIR" ]; then
      mkdir -p $WORK_DIR
      if [ ! $? -eq 0 ]; then
        echo "Work dir '"$WORK_DIR"' could not be created"
        exit 1
      fi
    fi
  else
    echo "No localdisk mounted"
    exit 1
  fi
fi
echo $WORK_DIR

export CMTCONFIG=x86_64-slc6-gcc49-opt
source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh
echo $CMTCONFIG

#  set the LHCbDirac environment
#. `which SetupProject.sh` LHCbDirac
#  set the certificate directories
export http_proxy=http://netgw01:8080
export X509_CERT_DIR=/cvmfs/lhcb.cern.ch/etc/grid-security/certificates/
export X509_VOMS_DIR=/cvmfs/lhcb.cern.ch/etc/grid-security/vomsdir
# check if the working directory exist
if ! [ -d "$WORK_DIR" ] ; then
  exit 1
fi
cd $WORK_DIR
if [ -d "$WORK_DIR/lhcb" ] ; then
  rm -rf $WORK_DIR/lhcb/*
fi

echo "Here I am " $PWD
echo "User: "$USER
#export CGSI_TRACE=1
# tmStart-ed environment is really bare need to define this
export PATH=/opt/WinCC_OA/3.11/bin:/sw/oracle/11.2.0.3.0/x86_64-slc6-gcc46-opt/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v8r4p2/InstallA
rea/scripts:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/Python/2.7.9.p1/x86_64-slc6-gcc48-opt/bin:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/gcc/4
.8.4/x86_64-slc6/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/COMPAT/COMPAT_v1r19/CompatSys/x86_64-slc6-gcc48-opt/bin:/cvmfs/lhcb.cern.ch/lib/contrib/CMT/v1r20
p20090520/Linux-x86_64:/cvmfs/lhcb.cern.ch/lib/contrib/CMake/2.8.9/Linux-i386/bin:/usr/local/bin:/bin:/usr/bin:/usr/sbin:/sbin:/opt/FMC/bin:/cvmfs/
lhcb.cern.ch/lib/lcg/external/ROOT/5.32.02/x86_64-slc6-gcc48-opt/root/bin:/opt/fmc/bin:/group/online/bin:/group/online/scripts:/home/lhcbprod/bin
#export CGSI_TRACEFILE=/localdisk/dirac/work/tracefile_$$.out
#export DO_NOT_DO_JOB_CLEANUP=True
#  measurement of the CPU normalisation of the machine
#export CPUNF=1
$HOME/production/dirac-pilot-2.sh --dirac-tmp=$WORK_DIR

