#!/bin/sh
#
# Runs as dirac. Sets up to run dirac-pilot.py
#

date --utc +"%Y-%m-%d %H:%M:%S %Z vm-pilot Start vm-pilot"
export HOME=/home/lhcbprod

export DIRAC_VERSION=v6r17p24
export DIRAC_VERSION=v6r17p31

export http_proxy=http://netgw01:8080
export https_proxy=http://netgw01:8080


cd $HOME/production/Pilot

#DIRAC_INSTALL='https://raw.githubusercontent.com/DIRACGrid/DIRAC/raw/integration/Core/scripts/dirac-install.py'
#DIRAC_PILOT='https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/WorkloadManagementSystem/PilotAgent/dirac-pilot.py'
#DIRAC_PILOT_TOOLS='https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/WorkloadManagementSystem/PilotAgent/pilotTools.py'
#DIRAC_PILOT_COMMANDS='https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/WorkloadManagementSystem/PilotAgent/pilotCommands.py'
#LHCbDIRAC_PILOT_COMMANDS='http://svn.cern.ch/guest/dirac/LHCbDIRAC/trunk/LHCbDIRAC/WorkloadManagementSystem/PilotAgent/LHCbPilotCommands.py'
LHCbDIRAC_PILOT_COMMANDS='https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/raw/master/LHCbDIRAC/WorkloadManagementSystem/PilotAgent/LHCbPilotCommands.py'
LHCBDIRAC_PILOT_JSON='http://cern.ch/lhcbproject/dist/Dirac_project/defaults/LHCb-pilot.json'

echo "Getting DIRAC Pilot 2.0 code from lhcbproject for now..."
#DIRAC_INSTALL='https://lhcbproject.web.cern.ch/lhcbproject/Operations/VM/pilotscripts/dirac-install.py'
#DIRAC_PILOT='https://lhcbproject.web.cern.ch/lhcbproject/Operations/VM/pilot2/dirac-pilot.py'
#DIRAC_PILOT_TOOLS='https://lhcbproject.web.cern.ch/lhcbproject/Operations/VM/pilot2/pilotTools.py'
#DIRAC_PILOT_COMMANDS='https://lhcbproject.web.cern.ch/lhcbproject/Operations/VM/pilot2/pilotCommands.py'
#DIRAC_INSTALL='https://lhcbproject.web.cern.ch/lhcbproject/Operations/VM/pilotscripts/dirac-install.py'
DIRAC_INSTALL="/cvmfs/lhcb.cern.ch/lib/lhcb/DIRAC/DIRAC_$DIRAC_VERSION/DIRAC/Core/scripts/dirac-install.py"
DIRAC_PILOT='/cvmfs/lhcb.cern.ch/lib/lhcb/DIRAC/DIRAC_'$DIRAC_VERSION'/DIRAC/WorkloadManagementSystem/PilotAgent/dirac-pilot.py'
DIRAC_PILOT_TOOLS='/cvmfs/lhcb.cern.ch/lib/lhcb/DIRAC/DIRAC_'$DIRAC_VERSION'/DIRAC/WorkloadManagementSystem/PilotAgent/pilotTools.py'
DIRAC_PILOT_COMMANDS='/cvmfs/lhcb.cern.ch/lib/lhcb/DIRAC/DIRAC_'$DIRAC_VERSION'/DIRAC/WorkloadManagementSystem/PilotAgent/pilotCommands.py'

#
##get the necessary scripts
#wget --no-check-certificate -O dirac-install.py $DIRAC_INSTALL
cp $DIRAC_INSTALL dirac-install.py
cp $DIRAC_PILOT dirac-pilot.py
cp $DIRAC_PILOT_TOOLS pilotTools.py
cp $DIRAC_PILOT_COMMANDS pilotCommands.py
wget --no-check-certificate -O LHCbPilotCommands.py $LHCbDIRAC_PILOT_COMMANDS

cd $HOME/production/etc
wget --no-check-certificate -O LHCb-pilot.json $LHCBDIRAC_PILOT_JSON
